## Run the app locally

##### Clone a repository and update .env file

- git clone https://gitlab.com/peter4478/smartlook-backend-test-project.git
- update the mysql settings in the .env file

##### Create SQL tables

```sql
CREATE TABLE stories (
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	userName varchar(255) NOT NULL,
	dateReg datetime DEFAULT CURRENT_TIMESTAMP,
	title varchar(255),
	body text,
	score int(11)
);
CREATE TABLE posts (
	id int(11) PRIMARY KEY AUTO_INCREMENT,
	idStory int(11) NOT NULL,
	userName varchar(255) NOT NULL,
	dateReg datetime DEFAULT CURRENT_TIMESTAMP,
	title varchar(255),
	body text,
	score int(11)
)
```

##### Install dependencies and start the server

- `npm install`
- `npm start`

##### Testing

- `npm run test`
