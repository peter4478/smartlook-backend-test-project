import { Request, Response } from "express";
import { _Query } from "../config/mysql";

const tableName = "stories";

/** INSERT */
const insert = async (req: Request, res: Response) => {
	let { userName, title, body, score } = req.body;
	let query = `
        INSERT INTO ${tableName} (userName, title, body, score)
        VALUES ("${userName}", "${title}", "${body}", "${score}")
    `;
	return await _Query(query, req, res);
};

/** GET ALL */
const getAll = async (req: Request, res: Response) => {
	let query = `SELECT * FROM ${tableName}`;
	return await _Query(query, req, res);
};

/** GET BY ID */
const getById = async (req: Request, res: Response) => {
	let { id } = req.params;
	let query = `SELECT * FROM ${tableName} WHERE id = "${id}"`;
	return await _Query(query, req, res);
};

/** UPDATE */
const update = async (req: Request, res: Response) => {
	let { id, title, body, score } = req.body;
	let query = `
        UPDATE ${tableName} 
        SET title = "${title}", 
        body = "${body}",
        score = "${score}"
        WHERE id = "${id}"
    `;
	return await _Query(query, req, res);
};

/** DESTROY */
const destroy = async (req: Request, res: Response) => {
	let { id } = req.params;
	let query = `DELETE FROM ${tableName} WHERE ID = "${id}"`;
	return await _Query(query, req, res);
};

/** STATS */
const stats = async (req: Request, res: Response) => {
	let { userName } = req.body;
	let query = `
        SELECT AVG(score) as averageScore, COUNT(userName) AS sumPosts FROM ${tableName}
        WHERE userName = "${userName}"
        GROUP BY userName
    `;
	return await _Query(query, req, res);
};

export default {
	insert,
	getAll,
	getById,
	update,
	destroy,
	stats,
};
