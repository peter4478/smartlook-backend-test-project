import { Request, Response } from "express";
import { _Query } from "../config/mysql";

const tableName = "posts";

/** INSERT */
const insert = async (req: Request, res: Response) => {
	let { userName, idStory, title, body, score } = req.body;
	let query = `
        INSERT INTO ${tableName} (userName, idStory, title, body, score)
        VALUES ("${userName}", "${idStory}", "${title}", "${body}", "${score}")
    `;
	return await _Query(query, req, res);
};

/** UPDATE */
const update = async (req: Request, res: Response) => {
	let { id, title, body, score } = req.body;
	let query = `
        UPDATE ${tableName} 
        SET title = "${title}", 
        body = "${body}",
        score = "${score}"
        WHERE id = "${id}"
    `;
	return await _Query(query, req, res);
};

/** GET ALL */
const getPosts = async (req: Request, res: Response) => {
	let { idStory } = req.params;
	let query = `
		SELECT * FROM ${tableName}
		WHERE idStory = "${idStory}"
	`;
	return await _Query(query, req, res);
};

/** DESTROY */
const destroy = async (req: Request, res: Response) => {
	let { id } = req.params;
	let query = `DELETE FROM ${tableName} WHERE ID = "${id}"`;
	return await _Query(query, req, res);
};

export default {
	insert,
	getPosts,
	update,
	destroy,
};
