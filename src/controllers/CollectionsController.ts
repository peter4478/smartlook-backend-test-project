import { Request, Response } from "express";
import { Connect, Query } from "../config/mysql";

/** getStoryAndPosts */
const getStoryAndPosts = async (req: Request, res: Response) => {
	Connect()
		.then((connection) => {
			let { idStory } = req.params;
			let query = `
				SELECT * FROM stories
				WHERE id = "${idStory}"
			`;
			Query(connection, query)
				.then((story) => {
					query = `
						SELECT * FROM posts
						WHERE idStory = "${idStory}"
					`;
					Query(connection, query).then((posts) => {
						return res.status(200).json({
							story: story,
							posts: posts,
						});
					});
				})
				.catch((error) => {
					return res.status(401).json({
						message: error.message,
						error,
					});
				})
				.finally(() => {
					connection.end();
				});
		})
		.catch((error) => {
			return res.status(401).json({
				message: error.message,
				error,
			});
		});
};

/** searchStoriesAndPosts */
const searchStoriesAndPosts = async (req: Request, res: Response) => {
	Connect()
		.then((connection) => {
			let { searchValue } = req.params;
			let query = `
				SELECT * FROM stories				
				WHERE title LIKE "%${searchValue}%"
				OR body LIKE "%${searchValue}%"
			`;
			Query(connection, query)
				.then((story) => {
					query = query.replace("FROM stories", "FROM posts");
					Query(connection, query).then((posts) => {
						return res.status(200).json({
							stories: story,
							posts: posts,
						});
					});
				})
				.catch((error) => {
					return res.status(200).json({
						message: error.message,
						error,
					});
				})
				.finally(() => {
					connection.end();
				});
		})
		.catch((error) => {
			return res.status(200).json({
				message: error.message,
				error,
			});
		});
};

export default {
	getStoryAndPosts,
	searchStoriesAndPosts,
};
