import { Request, Response } from "express";
import jwt from "jsonwebtoken";

const login = (req: Request, res: Response) => {
	const user = { name: req.body.username };
	const accessToken = jwt.sign(user, <string>process.env.ACCESS_TOKEN_SECRET);
	res.status(200).json({ accessToken });
};

export default { login };
