import { Request, Response } from "express";
import mysql from "mysql";
import config from "./config";
import logging from "../config/logging";

/** Params */
const params = {
	user: config.mysql.user,
	password: config.mysql.pass,
	host: config.mysql.host,
	database: config.mysql.database,
};

const NAMESPACE = "MySQL";

/** Connect */
const Connect = async () =>
	new Promise<mysql.Connection>((resolve, reject) => {
		const connection = mysql.createConnection(params);

		connection.connect((error) => {
			if (error) {
				reject(error);
				return;
			}

			resolve(connection);
		});
	});

/** Query */
const Query = async (connection: mysql.Connection, query: string) =>
	new Promise((resolve, reject) => {
		connection.query(query, connection, (error, result) => {
			if (error) {
				reject(error);
				return;
			}

			resolve(result);
		});
	});

/** _Query */
const _Query = async (query: string, req: Request, res: Response) => {
	Connect()
		.then((connection) => {
			Query(connection, query)
				.then((result) => {
					return res.status(200).json({
						result,
					});
				})
				.catch((error) => {
					logging.error(NAMESPACE, error.message, error);
					return res.status(401).json({
						message: error.message,
						error,
					});
				})
				.finally(() => {
					connection.end();
				});
		})
		.catch((error) => {
			logging.error(NAMESPACE, error.message, error);
			return res.status(401).json({
				message: error.message,
				error,
			});
		});
};

export { Connect, Query, _Query };
