import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

const authenticateToken = async (req: Request, res: Response, next: NextFunction) => {
    const authHeader = <string>req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) res.sendStatus(401);

    jwt.verify(token, <string>process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) res.sendStatus(403);
        req.body.user = user;
        next();
    });
};

export { authenticateToken };
