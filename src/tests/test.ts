import request from "supertest";

const BASE_URL = "http://localhost:8002";
const ACCESS_TOKEN =
	"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiUGV0ZXIiLCJpYXQiOjE2MTI4OTUxMDl9.HO2ZwPpZJinyzPRfi15Aj-OtDj1hRh6w9rNEVTcvnMY";
let lastStory: any;
let lastPost: any;

/** Authorization[Login] */
describe("Authorization", () => {
	it("login => [LoginController] => POST[/api/login]", async () => {
		await request(BASE_URL)
			.post("/api/login")
			.set("Content-Type", "application/json; charset=utf-8")
			.send({ username: "Peter" })
			.expect(200);
	});
});

/** insert[Story] */
describe("Stories, Posts", () => {
	it("insert => [StoriesController] => POST[/api/stories]", async () => {
		await request(BASE_URL)
			.post("/api/stories")
			.set("Content-Type", "application/json; charset=utf-8")
			.send({
				userName: "userTest",
				title: "titleTest",
				body: "bodyTest",
				score: 0,
			})
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** getAll[Story] */
	it("getAll => [StoriesController] => GET[/api/stories]", async () => {
		const response = await request(BASE_URL)
			.get("/api/stories")
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
		let result = response.body.result;
		lastStory = result[result.length - 1];
	});

	/** update[Story] */
	it("update => [StoriesController] => PUT[/api/stories]", async () => {
		await request(BASE_URL)
			.put("/api/stories")
			.set("Content-Type", "application/json; charset=utf-8")
			.send({
				id: lastStory.id,
				title: "updated title",
				body: "updated text",
				score: "30",
			})
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** stats[Story] */
	it("stats => [StoriesController] => POST[/api/stories/stats]", async () => {
		await request(BASE_URL)
			.post("/api/stories/stats")
			.send({ userName: lastStory.userName })
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** insert[Post] */
	it("insert => [PostsController] => POST[/api/posts]", async () => {
		await request(BASE_URL)
			.post("/api/posts")
			.set("Content-Type", "application/json; charset=utf-8")
			.send({
				userName: lastStory.userName,
				idStory: lastStory.id,
				title: "titleTest",
				body: "bodyTest",
				score: 0,
			})
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** getPosts[Post] */
	it("getPosts => [PostsController] => GET[/api/posts/:idStory]", async () => {
		const response = await request(BASE_URL)
			.get("/api/posts/" + lastStory.id)
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
		let result = response.body.result;
		lastPost = result[result.length - 1];
	});

	/** update[Post] */
	it("update => [PostsController] => PUT[/api/posts]", async () => {
		await request(BASE_URL)
			.put("/api/posts")
			.set("Content-Type", "application/json; charset=utf-8")
			.send({
				id: lastPost.id,
				title: "updated title",
				body: "updated text",
				score: 0,
			})
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** getStoryAndPosts[Story, Posts] */
	it("getStoryAndPosts => [CollectionsController] => GET[/api/stories/:idStory]", async () => {
		await request(BASE_URL)
			.get("/api/stories/" + lastStory.id)
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** searchStoriesAndPosts[Stories, Posts] */
	it("searchStoriesAndPosts => [CollectionsController] => GET[/api/stories/search/:searchValue]", async () => {
		await request(BASE_URL)
			.get("/api/stories/search/test")
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** destroy[Story] */
	it("destroy => [StoriesController] => DELETE[/api/stories/:idStory]", async () => {
		await request(BASE_URL)
			.delete("/api/stories/" + lastStory.id)
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});

	/** destroy[post] */
	it("destroy => [PostController] => DELETE[/api/posts/:idStory]", async () => {
		await request(BASE_URL)
			.delete("/api/posts/" + lastPost.id)
			.set("Authorization", ACCESS_TOKEN)
			.expect(200);
	});
});
