import express from "express";
import StoriesController from "../controllers/StoriesController";
import CollectionsController from "../controllers/CollectionsController";

const router = express.Router();

router.post("/", StoriesController.insert);
router.put("/", StoriesController.update);
router.get("/", StoriesController.getAll);
router.get("/:idStory", CollectionsController.getStoryAndPosts);
router.delete("/:id", StoriesController.destroy);
router.post("/stats", StoriesController.stats);
router.get("/search/:searchValue", CollectionsController.searchStoriesAndPosts);

export = router;
