import express from "express";
import PostsController from "../controllers/PostsController";

const router = express.Router();

router.post("/", PostsController.insert);
router.put("/", PostsController.update);
router.get("/:idStory", PostsController.getPosts);
router.delete("/:id", PostsController.destroy);

export = router;
